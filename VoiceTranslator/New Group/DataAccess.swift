//
//  DataAccess.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 2/2/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import Foundation

class DataAccess {
    /* Please notice : DO NOT CHANGE THE VALUES */
    
    // support 62 voices for speeching recognition : SFSpeechRecognizer
    // with 34 languages in difference
    static var recentLanguages = [["en", "English - United States"]]
    static let languages = [["ar" , "Arabic - Saudi Arabia"], ["ca" , "Catalan - Catalan"], ["cs" , "Czech - Czech Republic"], ["da" , "Danish - Denmark"], ["de" , "German - Austria"], ["de", "German - Switzerland"], ["de" , "German - Germany"], ["el" , "Greek - Greece"], ["en" , "English - Arabic (U.A.E.)"], ["en" , "English - Australia"], ["en" , "English - Canada"], ["en" , "English - United Kingdom"], ["en" , "English - Indonesia"], ["en" , "English - Ireland"], ["en" , "English - India"], ["en" , "English - New Zealand"], ["en" , "English - Philippines"], ["en" , "English - Saudi Arabia"], ["en" , "English - Singapore"], ["en" , "English - United States"], ["en" , "English - South Africa"], ["es" , "Spanish - Chile"], ["es" , "Spanish - Colombia"], ["es" , "Spanish - Spain"], ["es" , "Spanish - Mexico"] , ["es" , "Spanish - United States"], ["fi" , "Finnish - Finland"], ["fr" , "French - Belgium"], ["fr" , "French - Canada"], ["fr" , "French - Switzerland"], ["fr" , "French - France"], ["iw" , "Hebrew - Israel"], ["hi" , "Hindi - India"], ["hi" , "Hindi - India translit"], ["hi" , "Hindi - Latin"], ["hr" , "Croatian - Croatia"], ["hu" , "Hungarian - Hungary"], ["id", "Indonesian - Indonesia"], ["it" , "Italian - Switzerland"], ["it" , "Italian - Italy"], ["ja" , "Japanese - Japan"], ["ko" , "Korean - Korea"], ["ms" , "Malay - Malaysia"], ["no" , "Norwegian - Norway"], ["nl" , "Dutch - Belgium"], ["nl" , "Dutch - Netherlands"], ["pl" , "Polish - Poland"], ["pt" , "Portuguese - Brazil"], ["pt" , "Portuguese - Portugal"], ["ro" , "Romanian - Romania"], ["ru" , "Russian - Russia"], ["sk" , "Slovak - Slovakia"], ["sv" , "Swedish - Sweden"], ["th" , "Thai - Thailand"], ["tr" , "Turkish - Turkey"], ["uk" , "Ukrainian - Ukraine"], ["vi" , "Vietnamese - Vietnam"], ["zh-CN" , "Wuu - China"], ["zh-CN" , "Yue - China"] , ["zh-CN" , "Chinese - China"], ["zh-CN" , "Chinese - Hong Kong"], ["zh-TW" , "Chinese - Taiwan"]]
    
    // Apple currently supports 37 different voices Text-to-speak : AVSpeechSynthesizerVoice
    static let availableLocales = ["ar-SA", "ca-ES", "cs-CZ", "da-DK", "de-AT", "de-CH", "de-DE", "el-GR", "en-AE", "en-AU", "en-CA", "en-GB", "en-ID", "en-IE", "en-IN", "en-NZ", "en-PH", "en-SA", "en-SG", "en-US", "en-ZA", "es-CL", "es-CO", "es-ES", "es-MX", "es-US", "fi-FI", "fr-BE", "fr-CA", "fr-CH", "fr-FR", "he-IL", "hi-IN", "hi-IN-translit", "hi-Latn", "hr-HR", "hu-HU", "id-ID", "it-CH", "it-IT", "ja-JP", "ko-KR", "ms-MY", "nb-NO", "nl-BE", "nl-NL", "pl-PL", "pt-BR", "pt-PT", "ro-RO", "ru-RU", "sk-SK", "sv-SE", "th-TH", "tr-TR", "uk-UA", "vi-VN", "wuu-CN", "yue-CN", "zh-CN", "zh-HK", "zh-TW"]
    /* DO NOT CHANGE THE VALUES */
    
    // arrayPairText: [srcText-srcCodeVoice-srcImageName-srcName - destText-destCodeVoice-destImageName-destName]
//    static var arrPairText : [[String]] = []
    
    // LanguageStatic for translating
//    static var srcLanguageStatic = [String]()
//    static var destLanguageStatic = [String]()
    
}

