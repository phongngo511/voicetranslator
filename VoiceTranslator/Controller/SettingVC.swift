//
//  SettingVC.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 2/2/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit
import Speech
import RealmSwift

class SettingVC: UITableViewController {
    
    @IBOutlet weak var rateValueLbl: UILabel!
    @IBOutlet weak var pitchValueLbl: UILabel!
    @IBOutlet weak var volumeValueLbl: UILabel!
    
    @IBOutlet weak var rateSlider: UISlider!
    @IBOutlet weak var pitchSlider: UISlider!
    @IBOutlet weak var volumeSlider: UISlider!
    
    @IBOutlet weak var detectEndOfSpeechSwitch: UISwitch!
    @IBOutlet weak var autoPronounceSwitch: UISwitch!
    @IBOutlet weak var useWifiOnlySwitch: UISwitch!
    
    @IBOutlet weak var deleteHistoryBtn: UIButton!
    
    private let speechSynthesizer = AVSpeechSynthesizer()
    var arrVoiceLanguages: [Dictionary<String, String>] = []
    let defaults = UserDefaults.standard
    
    private var realm: Realm!
    private var translatorList: Results<Translator> {
        get {
            return try! Realm().objects(Translator.self)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareVoiceList() // need to be sorted !!!
        realm = try! Realm()
        
        deleteHistoryBtn.layer.cornerRadius = 10
        deleteHistoryBtn.layer.masksToBounds = true
        
        // difference between SpeechRecognition & SpeechSynthesizerVoice
//        let difference = ["ca-ES", "de-AT", "de-CH", "en-AE", "en-CA", "en-ID", "en-IN", "en-NZ", "en-PH", "en-SA", "en-SG", "es-CL", "es-CO", "es-US", "fr-BE", "fr-CH", "hi-IN-translit", "hi-Latn", "hr-HR", "it-CH", "ms-MY", "nb-NO", "uk-UA", "vi-VN", "wuu-CN", "yue-CN"]
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        populateSettings()
        
        autoPronounDidChange(autoPronounceSwitch)
        detectEndOfSpeechDidChange(detectEndOfSpeechSwitch)
        UseOnlyWifiDidChange(useWifiOnlySwitch)
    }
    
    
    @IBAction func autoPronounDidChange(_ sender: UISwitch) {
        if sender.isOn {
            UserDefaults.standard.set(true, forKey: "autoPronoun")
        } else {
            UserDefaults.standard.set(false, forKey: "autoPronoun")
        }
    }
    
    @IBAction func detectEndOfSpeechDidChange(_ sender: UISwitch) {
        if sender.isOn {
            UserDefaults.standard.set(true, forKey: "detectEndOfSpeech")
        } else {
            UserDefaults.standard.set(false, forKey: "detectEndOfSpeech")
        }
    }
    @IBAction func UseOnlyWifiDidChange(_ sender: UISwitch) {
        if sender.isOn {
            UserDefaults.standard.set(true, forKey: "useOnlyWifi")
        } else {
            UserDefaults.standard.set(false, forKey: "useOnlyWifi")
        }
    }
    @IBAction func deleteHistoryDidPressed(_ sender: UIButton) {
        let alertVC = UIAlertController(title: "Are you sure you want to delete history", message: nil, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (_) in
            try! self.realm.write {
                self.realm.delete(self.translatorList)
            }
            
            let successText = UILabel(frame: CGRect(x: self.view.frame.width / 2 - 100 , y: self.view.frame.height / 2 - 30, width: 200, height: 60))
            successText.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
            successText.text = "Deleted history"
            successText.textColor = .white
            successText.textAlignment = .center
            successText.layer.cornerRadius = 10
            successText.layer.masksToBounds = true
            self.view.addSubview(successText)
            
            Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { (_) in
                successText.removeFromSuperview()
            }
        }))
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertVC, animated: true, completion: nil)
    }
    @IBAction func rateDidChange(_ sender: UISlider) {
        rateValueLbl.text = String(format: "%.2f", sender.value)
        defaults.set(sender.value, forKey: "rateValue")
    }
    
    @IBAction func pitchDidChange(_ sender: UISlider) {
        pitchValueLbl.text = String(format: "%.2f", sender.value)
        defaults.set(sender.value, forKey: "pitchValue")
    }
    @IBAction func volumeDidChange(_ sender: UISlider) {
        volumeValueLbl.text = String(format: "%.2f", sender.value)
        defaults.set(sender.value, forKey: "volumeValue")
    }
    static func registerDefaults() {
        if (UserDefaults.standard.object(forKey: "rateValue") == nil) {
            UserDefaults.standard.register(defaults: ["rateValue": 0.40])
        }
        if (UserDefaults.standard.object(forKey: "pitchValue") == nil) {
            UserDefaults.standard.register(defaults: ["pitchValue": 1.0])
        }
        if (UserDefaults.standard.object(forKey: "volumeValue") == nil) {
            UserDefaults.standard.register(defaults: ["volumeValue": 0.75])
        }
        if (UserDefaults.standard.object(forKey: "autoPronoun") == nil) {
            UserDefaults.standard.register(defaults: ["autoPronoun": true])
        }
        if (UserDefaults.standard.object(forKey: "detectEndOfSpeech") == nil) {
            UserDefaults.standard.register(defaults: ["detectEndOfSpeech": true])
        }
        if (UserDefaults.standard.object(forKey: "useOnlyWifi") == nil) {
            UserDefaults.standard.register(defaults: ["useOnlyWifi": true])
        }
        
    }
    fileprivate func populateSettings() {
        if defaults.object(forKey: "rateValue") != nil {
            rateSlider.value = defaults.float(forKey: "rateValue")
            rateValueLbl.text = String(format: "%.2f", defaults.float(forKey: "rateValue"))
        }
        if defaults.object(forKey: "pitchValue") != nil {
            pitchSlider.value = defaults.float(forKey: "pitchValue")
            pitchValueLbl.text = String(format: "%.2f", defaults.float(forKey: "pitchValue"))
        }
        if defaults.object(forKey: "volumeValue") != nil {
            volumeSlider.value = defaults.float(forKey: "volumeValue")
            volumeValueLbl.text = String(format: "%.2f", defaults.float(forKey: "volumeValue"))
        }
        if defaults.bool(forKey: "autoPronoun") {
            autoPronounceSwitch.isOn = true
        } else {
            autoPronounceSwitch.isOn = false
        }
        if defaults.bool(forKey: "detectEndOfSpeech") {
            detectEndOfSpeechSwitch.isOn = true
        } else {
            detectEndOfSpeechSwitch.isOn = false
        }
        if defaults.bool(forKey: "useOnlyWifi") {
            useWifiOnlySwitch.isOn = true
        } else {
            useWifiOnlySwitch.isOn = false
        }
    }
    
    func prepareVoiceList() {
        for voice in AVSpeechSynthesisVoice.speechVoices() {
            let voiceLanguageCode = (voice as AVSpeechSynthesisVoice).language
            let languageName = voice.name
            let dictionary = ["name": languageName, "languageCode": voiceLanguageCode]
            arrVoiceLanguages.append(dictionary)
        }
    }
    @IBAction func doneBtnPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
