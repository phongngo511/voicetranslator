//
//  UserSuggestionVC.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 3/17/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class UserSuggestionVC: UIViewController {
        
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var srcLanguageBtn: UIButton!
    @IBOutlet weak var srcLanguageLbl: UILabel!
    @IBOutlet weak var destLanguageBtn: UIButton!
    @IBOutlet weak var destLanguageLbl: UILabel!
    
    static var srcLanguage = ["en", "United States" ,"English - United States"]
    static var destLanguage = ["fr", "France" ,"French - France"]
    static var indexOfLocaleSrc = 19
    static var indexOfLocaleDest = 30
    var passedIdRealm : Int?
    var passedText: String?
    
    var userSuggestDelegate: UserSuggestionDelegate?
    var editUserSuggestDelegate: EditUserSuggestionDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupUIButton()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupUIButton()
        textView.text = passedText
    }
    
    @IBAction func doneBtnPressed(_ sender: UIButton) {
        if textView.text != "Type new sentence..." && textView.text != nil && textView.text != "" {
            userSuggestDelegate?.userDidAddSuggestion(srcString: UserSuggestionVC.srcLanguage, withSrcLocale: UserSuggestionVC.indexOfLocaleSrc, text: textView.text, destString: UserSuggestionVC.destLanguage, destLocale: UserSuggestionVC.indexOfLocaleDest)
            editUserSuggestDelegate?.userDidEditSuggestion(srcString: UserSuggestionVC.srcLanguage, withSrcLocale: UserSuggestionVC.indexOfLocaleSrc, text: textView.text, destString: UserSuggestionVC.destLanguage, destLocale: UserSuggestionVC.indexOfLocaleDest, idRealm: passedIdRealm!)
        }
        dismiss(animated: true, completion: nil)
    }
    @IBAction func srcLanguageBtnPressed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toLanguageVC") as? LanguageVC else { return }
        vc.srcDelegate = self
        present(vc, animated: true, completion: nil)
    }
    @IBAction func destLanguageBtnPressed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toLanguageVC") as? LanguageVC else { return }
        vc.destDelegate = self
        present(vc, animated: true, completion: nil)
    }
    func setupUI() {
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = UIColor.black.cgColor
        textView.layer.cornerRadius = 10
        textView.text = "Type new sentence..."
        textView.textColor = UIColor.lightGray
        textView.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(sender:)))
        view.addGestureRecognizer(tap)
    }
    func setupUIButton() {
        srcLanguageBtn.setImage(UIImage(named: UserSuggestionVC.srcLanguage[1]), for: .normal)
        srcLanguageLbl.text = UserSuggestionVC.srcLanguage[2]
        
        destLanguageBtn.setImage(UIImage(named: UserSuggestionVC.destLanguage[1]), for: .normal)
        destLanguageLbl.text = UserSuggestionVC.destLanguage[2]
    }
    @objc func dismissKeyboard(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
}
extension UserSuggestionVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Type new sentence..."
            textView.textColor = UIColor.lightGray
        }
    }
    func textViewDidChange(_ textView: UITextView) {
        let size = textView.sizeThatFits(textView.frame.size)
        if size.height > textView.frame.size.height {
            textView.frame.size.height = size.height
            textView.setContentOffset(CGPoint.zero, animated: false)
        }
        if size.height < textView.frame.size.height {
            textView.frame.size.height = 60
            textView.setContentOffset(CGPoint.zero, animated: false)
        }
    }
}
extension UserSuggestionVC: SourceLanguageDelegate {
    func passInfo(str: Array<String>,withLocale index: Int) {
        UserSuggestionVC.srcLanguage = str
        let code = str[0]
        let name = str[2]
        UserSuggestionVC.indexOfLocaleSrc = DataAccess.languages.index( where: { $0 == [code,name] })!
    }
}
extension UserSuggestionVC: DestinationLanguageDelegate {
    func passInformation(str: Array<String>,withLocale index: Int) {
        UserSuggestionVC.destLanguage = str
        let code = str[0]
        let name = str[2]
        UserSuggestionVC.indexOfLocaleDest = DataAccess.languages.index( where: { $0 == [code,name] })!
    }
}
