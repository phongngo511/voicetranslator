//
//  SuggestionVC
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 3/12/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit
import RealmSwift
protocol UserSuggestionDelegate {
    func userDidAddSuggestion(srcString: Array<String>,withSrcLocale srcIndex: Int, text: String, destString: Array<String>, destLocale destIndex: Int)
}
protocol EditUserSuggestionDelegate {
    func userDidEditSuggestion(srcString: Array<String>,withSrcLocale srcIndex: Int, text: String, destString: Array<String>, destLocale destIndex: Int, idRealm: Int)
}

protocol SuggestionDelagate {
    func didClickSuggestion(suggestion: UserSuggestion)
}

class SuggestionVC: UIViewController {

    @IBOutlet weak var suggestionTableView: UITableView!
    @IBOutlet weak var userSuggestionTableView: UITableView!
    @IBOutlet weak var srcLanguageLbl: UILabel!
    @IBOutlet weak var srcLanguageBtn: UIButton!
    @IBOutlet weak var destLanguageLbl: UILabel!
    @IBOutlet weak var destLanguageBtn: UIButton!
    @IBOutlet weak var voiceBtn: UIButton!
    @IBOutlet weak var mainView: UIView!
    
    // Offline
    @IBOutlet weak var offlineVoiceVisualView: UIVisualEffectView!
    @IBOutlet weak var offlineVoiceBtn: UIButton!
    @IBOutlet weak var youSaidLabel: UILabel!
    @IBOutlet weak var languageTF: UITextField!
    @IBOutlet weak var offlineResultBtn: UIButton!
    
    var languagePicker = UIPickerView()
    var selectedLanguage = "English"
    let languageArray = ["English","French" ,"Spanish", "Dutch", "German", "Italian"]
    
    // Suggestions
    var suggestArray = [String]()
    let suggestionCellId = "suggestionCell"
    static var srcLanguage = ["en", "United States" ,"English - United States"]
    static var destLanguage = ["fr", "France" ,"French - France"]
    static var indexOfLocaleSrc = 19
    static var indexOfLocaleDest = 30
    
    // User Suggestions
    let userCellId = "userSuggestionCell"
    
    var suggestionDelegate: SuggestionDelagate?
    
    private var realm: Realm!
    private var dataList: Results<UserSuggestion> {
        get {
            return try! Realm().objects(UserSuggestion.self)
        }
    }
    
    // OpenEars
    let lmGenerator = OELanguageModelGenerator()
    var openEarsEventsObserver = OEEventsObserver()
    
    var vocabulary: Array<String> = [] // These can be lowercase, uppercase, or mixed-case.
    var bundleModelName = "AcousticModelEnglish"
    
    var firstVocabularyName = "FirstVocabularyEnglish"
    var firstLanguageModelPath: String!
    var pathToFirstDynamicallyGeneratedDictionary: String!
    
    var secondVocabularyName = "SecondVocabularyEnglish"
    var secondLanguageModelPath: String!
    var secondGeneratedDictionaryPath: String!
    
    // Mark: - override funcs
    override func viewDidLoad() {
        super.viewDidLoad()
        
        realm = try! Realm()
        openEarsEventsObserver.delegate = self
        bundleModelName = "AcousticModelEnglish"
        
        buildData()
        setupUI()
        setupUIButton()

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        translateUserSuggestions()
        setupUIButton()
        vocabulary = addWords(forLanguage: selectedLanguage)
        
        realm.refresh()
        userSuggestionTableView.reloadData()
        suggestionTableView.reloadData()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        translateUserSuggestions()
    }
    // Mark: - IBAction
    @IBAction func doneBtnPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func addSuggestBtnPressed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toUserSuggestionVC") as? UserSuggestionVC else { return }
        vc.userSuggestDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func srcLanguageBtnPressed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toLanguageVC") as? LanguageVC else { return }
        vc.srcDelegate = self
        present(vc, animated: true, completion: nil)
    }
    @IBAction func destLanguageBtnPressed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toLanguageVC") as? LanguageVC else { return }
        vc.destDelegate = self
        present(vc, animated: true, completion: nil)
    }
    @IBAction func changeBtnPressed(_ sender: UIButton) {
        let src = SuggestionVC.destLanguage
        let dest = SuggestionVC.srcLanguage
        SuggestionVC.srcLanguage = src
        SuggestionVC.destLanguage = dest
        let index = SuggestionVC.indexOfLocaleDest
        SuggestionVC.indexOfLocaleDest = SuggestionVC.indexOfLocaleSrc
        SuggestionVC.indexOfLocaleSrc = index

        let newIndex = src[2].index(of: "-")!
        let newName = src[2].prefix(upTo: newIndex).dropLast()
        suggestArray = readDataFromFile(name: String(newName), ofType: "txt", separatedBy: "\n")
        
        suggestionTableView.reloadData()
        setupUIButton()
    }
    
    @IBAction func offlineVoiceBtnPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        if !OEPocketsphinxController.sharedInstance().isListening {
            youSaidLabel.text = ""
            let firstLanguageArray = vocabulary
            
            
            let firstLanguageModelGenerationError: Error! = lmGenerator.generateLanguageModel(from: firstLanguageArray, withFilesNamed: firstVocabularyName, forAcousticModelAtPath: OEAcousticModel.path(toModel: bundleModelName))
            
            if(firstLanguageModelGenerationError != nil) {
                print("Error while creating initial language model: \(firstLanguageModelGenerationError)")
            } else {
                self.firstLanguageModelPath = lmGenerator.pathToSuccessfullyGeneratedLanguageModel(withRequestedName: firstVocabularyName)
                self.pathToFirstDynamicallyGeneratedDictionary = lmGenerator.pathToSuccessfullyGeneratedDictionary(withRequestedName: firstVocabularyName)
                
                let secondLanguageArray = vocabulary
                
                let secondLanguageModelGenerationError: Error! = lmGenerator.generateLanguageModel(from: secondLanguageArray, withFilesNamed: secondVocabularyName, forAcousticModelAtPath: OEAcousticModel.path(toModel: bundleModelName))
                
                if(secondLanguageModelGenerationError != nil) {
                    print("Error while creating second language model: \(secondLanguageModelGenerationError)")
                } else {
                    self.secondLanguageModelPath = lmGenerator.pathToSuccessfullyGeneratedLanguageModel(withRequestedName: secondVocabularyName)
                    self.secondGeneratedDictionaryPath = lmGenerator.pathToSuccessfullyGeneratedDictionary(withRequestedName: secondVocabularyName)
                    do {
                        try OEPocketsphinxController.sharedInstance().setActive(true)
                    }
                    catch {
                        print("Error: it wasn't possible to set the shared instance to active: \"\(error)\"")
                    }
                    
                    if(!OEPocketsphinxController.sharedInstance().isListening) {
                        OEPocketsphinxController.sharedInstance().startListeningWithLanguageModel(atPath: self.firstLanguageModelPath, dictionaryAtPath: self.pathToFirstDynamicallyGeneratedDictionary, acousticModelAtPath: OEAcousticModel.path(toModel: bundleModelName), languageModelIsJSGF: false)
                    }
                }
            }
            offlineVoiceBtn.alpha = 0.2
        }
    }
    
    @IBAction func voiceBtnPressed(_ sender: UIButton) {
        UIView.animate(withDuration: 0.5) {
            self.offlineVoiceVisualView.isHidden = false
            self.mainView.alpha = 0.8
        }
        
    }
    @IBAction func offlineResultBtnPressed(_ sender: UIButton) {
        let text = sender.titleLabel?.text
        
        // User-defined data
        for i in dataList {
            if i.sourceText == text {
                suggestionDelegate?.didClickSuggestion(suggestion: i)
                dismiss(animated: true, completion: nil)
            }
        }
        
        // Dev-Defined data
        let dataFromFile = readDataFromFile(name: selectedLanguage, ofType: "txt", separatedBy: "\n")
        for k in 0..<dataFromFile.count-1 {
            if dataFromFile[k] == text {
                
                let lang = ["English","Spanish","French","German","Italian","Dutch"]
                let srcCodeVoiceArr = ["en-US","es-ES","fr-FR","de-DE","it-IT","nl-NL"]
                let srcNameArr = ["English - United States","Spanish - Spain","French - France","German - Germany","Italian - Italy","Dutch - Netherlands"]
                let srcImgNameArr = ["United States", "Spain","France","Germany","Italy","Netherlands"]
                let srcLangTranslateArr = ["en","es","fr","de","it","nl"]
                
                let suggestion = UserSuggestion()
                
                let srcArray = readDataFromFile(name: selectedLanguage, ofType: "txt", separatedBy: "\n")
                suggestion.sourceText = srcArray[k]
                for i in 0..<lang.count-1 {
                    if selectedLanguage == lang[i] {
                        suggestion.sourceCodeVoice = srcCodeVoiceArr[i]
                        suggestion.sourceName = srcNameArr[i]
                        suggestion.sourceImageName = srcImgNameArr[i]
                        suggestion.sourceLanguageTranslate = srcLangTranslateArr[i]
                    }
                }
                
                let destIndex = SuggestionVC.destLanguage[2].index(of: "-")!
                let destName = SuggestionVC.destLanguage[2].prefix(upTo: destIndex).dropLast()
                let destArray = readDataFromFile(name: String(destName), ofType: "txt", separatedBy: "\n")
                suggestion.destinationText = destArray[k]
                suggestion.destinationCodeVoice = DataAccess.availableLocales[DataAccess.languages.index( where: { $0 == [SuggestionVC.destLanguage[0],SuggestionVC.destLanguage[2]] })!]
                suggestion.destinationImageName = SuggestionVC.destLanguage[1]
                suggestion.destinationName = SuggestionVC.destLanguage[2]
                suggestion.destinationLanguageTranslate = SuggestionVC.destLanguage[0]
                suggestionDelegate?.didClickSuggestion(suggestion: suggestion)
                
                dismiss(animated: true, completion: nil)
            }
        }
    }
    
    // Mark: - Functions
    func addWords(forLanguage lang: String) -> [String] {
        var array = [String]()
        
        // vocabulary from User-defined suggestions
        for i in 0..<dataList.count {
            let newIndex = dataList[i].sourceName.index(of: "-")!
            let newName = dataList[i].sourceName.prefix(upTo: newIndex).dropLast()
            
            if newName == lang {
                let words = dataList[i].sourceText.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "").replacingOccurrences(of: "?", with: "").components(separatedBy: " ")
                for k in words {
                    // remove duplicate element
                    if !array.contains(k.lowercased()) {
                        array.append(k.lowercased())
                    }
                }
            }
        }
        
        // vocabulary from Dev's suggestions
        let selectedLanguageArray = readDataFromFile(name: lang, ofType: "txt", separatedBy: "\n")
        for i in selectedLanguageArray {
            let words = i.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: ".", with: "").replacingOccurrences(of: "?", with: "").components(separatedBy: " ")
            for k in words {
                // remove duplicate element
                if !array.contains(k.lowercased()) {
                    array.append(k.lowercased())
                }
            }
        }
        // delete and sort
        array = array.filter({$0 != ""})
        array = array.filter({$0 != "?"})
        array = array.filter({$0 != "!"})
        array = array.filter({$0 != "/"})
        array.sort()
        return array
    }
    
    func setupUI() {
        userSuggestionTableView.delegate = self
        userSuggestionTableView.dataSource = self
        
        suggestionTableView.delegate = self
        suggestionTableView.dataSource = self
        
        offlineVoiceVisualView.layer.cornerRadius = 10
        offlineVoiceVisualView.layer.masksToBounds = true
        
        offlineResultBtn.layer.cornerRadius = 10
        offlineResultBtn.layer.masksToBounds = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleDismissOfflineVisualView(sender:)))
        self.mainView.addGestureRecognizer(tap)
        
        let endEditTap = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        endEditTap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(endEditTap)
        
        languagePicker.delegate = self
        languagePicker.dataSource = self
        
        let toolbarTime = UIToolbar();
        toolbarTime.sizeToFit()
        let done = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneTimePicker));
        let space = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelTimePicker));
        toolbarTime.setItems([cancel,space,done], animated: false)
        
        languagePicker.frame = CGRect(x: 0, y: 216, width: Int(view.frame.size.width), height: 216)
        languageTF.inputAccessoryView = toolbarTime
        languageTF.inputView = languagePicker
    }
    @objc func doneTimePicker(){
        languageTF.text = selectedLanguage
        self.view.endEditing(true)
    }
    @objc func cancelTimePicker(){
        self.view.endEditing(true)
    }
    
    @objc func handleDismissOfflineVisualView(sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.5) {
            self.offlineVoiceVisualView.isHidden = true
            self.mainView.alpha = 0
        }
        self.view.endEditing(true)
        OEPocketsphinxController.sharedInstance().stopListening()
        offlineVoiceBtn.alpha = 1
        youSaidLabel.text = ""
    }
    @objc func endEditing() {
        self.view.endEditing(true)
    }
    func setupUIButton() {
        srcLanguageBtn.setImage(UIImage(named: SuggestionVC.srcLanguage[1]), for: .normal)
        srcLanguageLbl.text = SuggestionVC.srcLanguage[2]
        
        destLanguageBtn.setImage(UIImage(named: SuggestionVC.destLanguage[1]), for: .normal)
        destLanguageLbl.text = SuggestionVC.destLanguage[2]
    }
    func buildData() {
        let newIndex = SuggestionVC.srcLanguage[2].index(of: "-")!
        let newName = SuggestionVC.srcLanguage[2].prefix(upTo: newIndex).dropLast()
        suggestArray = readDataFromFile(name: String(newName), ofType: "txt", separatedBy: "\n")
    }
    
    func translateUserSuggestions() {
        OperationQueue.main.addOperation {
            for i in self.dataList {
                if i.destinationText == "" {
                    TranslationService.shared.translate(text: i.sourceText, src: i.sourceLanguageTranslate, target: i.destinationLanguageTranslate, completion: { (success) in
                        if success {
                            try! self.realm.write {
                                let sug = i
                                sug.destinationText = TranslationService.shared.translatedText
                                self.realm.add(sug, update: true)
                            }
                        }
                    })
                    
                }
            }
            self.realm.refresh()
            self.userSuggestionTableView.reloadData()
        }
    }
    
    private func listFileInResources() -> [String] {
        var returnArray = [String]()
        if let directoryAndFileName = try? FileManager.default.contentsOfDirectory(atPath: Bundle.main.resourcePath!) {
            let results = directoryAndFileName.filter({ (fileName) -> Bool in
                let index = fileName.index(fileName.endIndex, offsetBy: -5)
                if fileName[index...].contains("txt") {
                    return true
                } else { return false }
            })
            returnArray = results
        } else {
            print("Bundle empty !")
        }
        return returnArray
    }
    
    private func readDataFromFile(name: String, ofType: String, separatedBy str: String ) -> [String] {
        var dataArray = [""]
        do {
            let path = Bundle.main.path(forResource: name, ofType: ofType)!
            let file = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
            dataArray = file.components(separatedBy: str)
        } catch {
            print("Fatal Error: Couldn't read the contens of file \(name).\(ofType) !")
        }
        return dataArray
    }
}
extension SuggestionVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == userSuggestionTableView {
            return dataList.count
        } else if tableView == suggestionTableView {
            return suggestArray.count
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == userSuggestionTableView {
            guard let userCell = tableView.dequeueReusableCell(withIdentifier: userCellId, for: indexPath) as? UserSuggestionCell else { return UITableViewCell() }
            userCell.sentenceLbl.text = dataList[indexPath.row].sourceText
            if dataList[indexPath.row].destinationText == "" {
                userCell.iconImageView.image = UIImage(named: "alert")
            } else {
                userCell.iconImageView.image = UIImage(named: "tick")
            }
            return userCell
        } else if tableView == suggestionTableView {
            let suggestionCell = UITableViewCell(style: .default, reuseIdentifier: suggestionCellId)
            suggestionCell.textLabel?.text = suggestArray[indexPath.row]
            suggestionCell.textLabel?.numberOfLines = 5
            return suggestionCell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == userSuggestionTableView {
            suggestionDelegate?.didClickSuggestion(suggestion: dataList[indexPath.row])
            dismiss(animated: true, completion: nil)
        } else if tableView == suggestionTableView {
            let suggestion = UserSuggestion()
            suggestion.sourceText = suggestArray[indexPath.row]
            suggestion.sourceCodeVoice = DataAccess.availableLocales[DataAccess.languages.index( where: { $0 == [SuggestionVC.srcLanguage[0],SuggestionVC.srcLanguage[2]] })!]
            suggestion.sourceImageName = SuggestionVC.srcLanguage[1]
            suggestion.sourceName = SuggestionVC.srcLanguage[2]
            suggestion.sourceLanguageTranslate = SuggestionVC.srcLanguage[0]
            
            let newIndex = SuggestionVC.destLanguage[2].index(of: "-")!
            let newName = SuggestionVC.destLanguage[2].prefix(upTo: newIndex).dropLast()
            let destArray = readDataFromFile(name: String(newName), ofType: "txt", separatedBy: "\n")
            suggestion.destinationText = destArray[indexPath.row]
            suggestion.destinationCodeVoice = DataAccess.availableLocales[DataAccess.languages.index( where: { $0 == [SuggestionVC.destLanguage[0],SuggestionVC.destLanguage[2]] })!]
            suggestion.destinationImageName = SuggestionVC.destLanguage[1]
            suggestion.destinationName = SuggestionVC.destLanguage[2]
            suggestion.destinationLanguageTranslate = SuggestionVC.destLanguage[0]
            
            suggestionDelegate?.didClickSuggestion(suggestion: suggestion)
            dismiss(animated: true, completion: nil)
        }
    }
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if tableView == userSuggestionTableView {
            let edit = UITableViewRowAction(style: .normal, title: "Edit") { (action, index) in
                guard let vc = self.storyboard?.instantiateViewController(withIdentifier: "toUserSuggestionVC") as? UserSuggestionVC else { return }
                vc.passedIdRealm = self.dataList[indexPath.row].id
                vc.passedText = self.dataList[indexPath.row].sourceText
                vc.editUserSuggestDelegate = self
                self.present(vc, animated: true, completion: nil)
            }
            let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, index) in
                try! self.realm.write {
                    self.realm.delete(self.dataList[indexPath.row])
                }
                self.realm.refresh()
                self.userSuggestionTableView.reloadData()
            }
            return [delete,edit]
        }
        return nil
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if tableView == suggestionTableView {
            return false
        }
        return true
    }
}
extension SuggestionVC: SourceLanguageDelegate {
    func passInfo(str: Array<String>,withLocale index: Int) {
        SuggestionVC.srcLanguage = str
        let code = str[0]
        let name = str[2]
        SuggestionVC.indexOfLocaleSrc = DataAccess.languages.index( where: { $0 == [code,name] })!
        
        let newIndex = str[2].index(of: "-")!
        let newName = str[2].prefix(upTo: newIndex).dropLast()
        suggestArray = readDataFromFile(name: String(newName), ofType: "txt", separatedBy: "\n").map({ $0.replacingOccurrences(of: "\'", with: "'")})
    }
}
extension SuggestionVC: DestinationLanguageDelegate {
    func passInformation(str: Array<String>, withLocale index: Int) {
        SuggestionVC.destLanguage = str
        let code = str[0]
        let name = str[2]
        SuggestionVC.indexOfLocaleDest = DataAccess.languages.index( where: { $0 == [code,name] })!
    }
}
extension SuggestionVC: UserSuggestionDelegate {
    
    func userDidAddSuggestion(srcString: Array<String>, withSrcLocale srcIndex: Int, text: String, destString: Array<String>, destLocale destIndex: Int) {
        
        let userSuggestion = UserSuggestion()
        try! self.realm.write {
            
            let nextId = self.realm.objects(UserSuggestion.self).max(ofProperty: "id") as Int? ?? 0
            userSuggestion.id = nextId + 1
            userSuggestion.sourceText = text
            userSuggestion.sourceCodeVoice = DataAccess.availableLocales[DataAccess.languages.index( where: { $0 == [srcString[0],srcString[2]] })!]
            userSuggestion.sourceImageName = srcString[1]
            userSuggestion.sourceName = srcString[2]
            userSuggestion.sourceLanguageTranslate = srcString[0]
            
            userSuggestion.destinationText = ""
            userSuggestion.destinationCodeVoice = DataAccess.availableLocales[DataAccess.languages.index( where: { $0 == [destString[0],destString[2]] })!]
            userSuggestion.destinationImageName = destString[1]
            userSuggestion.destinationName = destString[2]
            userSuggestion.destinationLanguageTranslate = destString[0]
            
             self.realm.add(userSuggestion, update: true)
            
        }
        TranslationService.shared.translate(text: text, src: srcString[0], target: destString[0]) { (success) in
            if success {
                try! self.realm.write {
                    userSuggestion.destinationText = TranslationService.shared.translatedText
                    self.realm.add(userSuggestion, update: true)
                }
            }
        }
    }
}
extension SuggestionVC: EditUserSuggestionDelegate {
    func userDidEditSuggestion(srcString: Array<String>, withSrcLocale srcIndex: Int, text: String, destString: Array<String>, destLocale destIndex: Int, idRealm: Int) {
        let userSuggestion = UserSuggestion()
        userSuggestion.id = idRealm
        userSuggestion.sourceText = text
        userSuggestion.sourceCodeVoice = DataAccess.availableLocales[DataAccess.languages.index( where: { $0 == [srcString[0],srcString[2]] })!]
        userSuggestion.sourceImageName = srcString[1]
        userSuggestion.sourceName = srcString[2]
        userSuggestion.sourceLanguageTranslate = srcString[0]
        
        userSuggestion.destinationText = ""
        userSuggestion.destinationCodeVoice = DataAccess.availableLocales[DataAccess.languages.index( where: { $0 == [destString[0],destString[2]] })!]
        userSuggestion.destinationImageName = destString[1]
        userSuggestion.destinationName = destString[2]
        userSuggestion.destinationLanguageTranslate = destString[0]
        try! self.realm.write {
            self.realm.add(userSuggestion, update: true)
        }
        TranslationService.shared.translate(text: text, src: srcString[0], target: destString[0]) { (success) in
            if success {
                try! self.realm.write {
                    userSuggestion.destinationText = TranslationService.shared.translatedText
                    self.realm.add(userSuggestion, update: true)
                }
            }
        }
    }
}

extension SuggestionVC: OEEventsObserverDelegate {
    func pocketsphinxDidReceiveHypothesis(_ hypothesis: String!, recognitionScore: String!, utteranceID: String!) {
        youSaidLabel.text = hypothesis
        
        // Search on list Sentences, match-up every sentences with the most similarity
        var userDefinedData = [String]()
        var devDefinedData = [String]()
        var resultsData = [String]()
        
        devDefinedData = readDataFromFile(name: selectedLanguage, ofType: "txt", separatedBy: "\n")
        for i in 0..<dataList.count {
            let newIndex = dataList[i].sourceName.index(of: "-")!
            let newName = dataList[i].sourceName.prefix(upTo: newIndex).dropLast()
            if newName == selectedLanguage {
                if !userDefinedData.contains(dataList[i].sourceText) {
                    userDefinedData.append(dataList[i].sourceText)
                }
            }
        }
        if let str = youSaidLabel.text {
            for i in userDefinedData {
                if levDis(w1: i, w2: str) < 15 {
                    if !resultsData.contains(i) {
                        resultsData.append(i)
                    }
                }
            }
            for i in devDefinedData {
                if levDis(w1: i, w2: str) < 15 {
                    if !resultsData.contains(i) {
                        resultsData.append(i)
                    }
                }
            }
        }
        if let titleforBtn = resultsData.first {
            offlineResultBtn.setTitle(titleforBtn, for: .normal)
        }
    }
    func pocketsphinxFailedNoMicPermissions() {
        let alert = UIAlertController(title: "Mic's permissions denied", message: "The user has never set mic permissions or denied permission to this app's mic, so listening will not start.", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: "App-Prefs:root=General") else { return }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
            }
        }
        alert.addAction(settingsAction)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
        
        if OEPocketsphinxController.sharedInstance().isListening {
            let error = OEPocketsphinxController.sharedInstance().stopListening() // Stop listening if we are listening.
            if(error != nil) {
                print("Error while stopping listening in micPermissionCheckCompleted: ", error!)
            }
        }
    }
    func pocketsphinxDidDetectFinishedSpeech() {
        OEPocketsphinxController.sharedInstance().stopListening()
        offlineVoiceBtn.alpha = 1
        
    }
    // Levenshtein Distance algorithm
    func levDis(w1: String, w2: String) -> Int {
        let (t, s) = (w1, w2)
        let empty = Array<Int>(repeating:0, count: s.count)
        var last = [Int](0...s.count)
        for (i, tLett) in t.enumerated() {
            var cur = [i + 1] + empty
            for (j, sLett) in s.enumerated() {
                cur[j + 1] = tLett == sLett ? last[j] : min(last[j], last[j + 1], cur[j])+1
            }
            last = cur
        }
        return last.last!
    }
}
extension SuggestionVC: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languageArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return languageArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedLanguage = languageArray[row]
        languageTF.text = selectedLanguage
        bundleModelName = "AcousticModel" + selectedLanguage
        firstVocabularyName = "FirstVocabulary" + selectedLanguage
        secondVocabularyName = "SecondVocabulary" + selectedLanguage
        vocabulary = addWords(forLanguage: selectedLanguage)
    }
}
