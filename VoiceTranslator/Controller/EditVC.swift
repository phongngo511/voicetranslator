//
//  EditVC.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 2/6/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

protocol EditTextDelegate {
    func editDone(withText text: String)
}

class EditVC: UIViewController {

    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var countryNameLbl: UILabel!
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var corneredView: UIView!
    @IBOutlet weak var textView: UITextView!
    
    var text: String!
    var countryName: String!
    var flagName: UIImage!
    
    var editDelegate: EditTextDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(sender:)))
        view.addGestureRecognizer(tap)
    }
    
    private func setupUI() {
        textView.text = text
        textView.delegate = self
        countryNameLbl.text = countryName
        flagImageView.image = flagName
        
        corneredView.layer.masksToBounds = true
        corneredView.layer.cornerRadius = 10
        if #available(iOS 11.0, *) {
            corneredView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        } else {
            corneredView.roundCorners(corners: [.topLeft, .topRight], radius: 10)
        }
        
        textView.layer.masksToBounds = true
        textView.layer.cornerRadius = 10
        if #available(iOS 11.0, *) {
            textView.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        } else {
            textView.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 10)
        }
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "screenShot")!)
    }
    
    @objc func dismissKeyboard(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    @IBAction func doneBtnPressed(_ sender: UIButton) {
        self.view.endEditing(true)
        editDelegate.editDone(withText: textView.text!)
        dismiss(animated: true, completion: nil)
    }
    
}
extension EditVC: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let size = textView.sizeThatFits(textView.frame.size)
        if size.height > textView.frame.size.height {
            textView.frame.size.height = size.height
            textView.setContentOffset(CGPoint.zero, animated: false)
        }
        if size.height < textView.frame.size.height {
            textView.frame.size.height = 60
            textView.setContentOffset(CGPoint.zero, animated: false)
        }
    }
}
