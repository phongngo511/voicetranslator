//
//  FullScreenVC.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 2/6/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class FullScreenVC: UIViewController {

    var text: String!
    
    @IBAction func doneBtnPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var textLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        textLbl.text = text
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override var shouldAutorotate: Bool {
        return false
    }
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return UIInterfaceOrientation.landscapeLeft
    }
}
