//
//  ViewController.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 2/2/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit
import Speech
import Reachability
import RealmSwift

protocol SourceLanguageDelegate {
    func passInfo(str: Array<String>,withLocale index: Int)
}
protocol DestinationLanguageDelegate {
    func passInformation(str: Array<String>,withLocale index: Int)
}

public class MainVC: UIViewController {
    
    // MARK: Properties
    private var speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))!
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var recognitionTask: SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    private let speechSynthesizer = AVSpeechSynthesizer()
    
    @IBOutlet var recordButton : GlowingButton!
    @IBOutlet weak var exchangeLangBtn: UIButton!
    @IBOutlet weak var recordView: UIView!
    
    @IBOutlet weak var firstButton: RoundedButton!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondButton: RoundedButton!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var suggestBtn: UIButton!
    @IBOutlet weak var settingBtn: UIButton!
    @IBOutlet weak var keyboardBtn: UIButton!
    
    // keyboard text
    @IBOutlet weak var keyboardView: UIView!
    @IBOutlet weak var keyboardTF: UITextField!
    @IBOutlet weak var keyboardSendBtn: UIButton!
    
    
    @IBOutlet weak var tableView: UITableView!
    let cellId = "translateCell"
    
    // Code, imageName, Name  ( dynamically )
    private var srcLanguage = ["en", "United States" ,"English - United States"]
    private var destLanguage = ["fr", "France" ,"French - France"]
    
    // For speechRegcognizer
    private var indexOfLocaleSrc = 19   // Default: index of "en-US" in availableLocals array for SrcLanguage
    private var indexOfLocaleDest = 30  // Default: index of "fr-FR" in availableLocals array for DestLanguage
    var originalText = ""
    var translatedText = ""
    
    // Edit text
    private var editedText: String?
    private var indexEditingText: IndexPath?
    
    // Reachability network : Cellular / Wifi
    let reachability = Reachability()!
    var isConnectionSatisfied = false
    
    // Detect end of speeching
    var timer: Timer?
    var speechTimeOutInterval: TimeInterval = 1.5 {
        didSet {
            restartSpeechTimeOut()
        }
    }
    
    // Realm for storing data
    private var realm: Realm!
    private var translatorList: Results<Translator> {
        get {
            return try! Realm().objects(Translator.self)
        }
    }

    
    // ------- MARK: UIViewController
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        recordButton.isEnabled = false
        recordButton.showsTouchWhenHighlighted = true
        
        keyboardBtn.isEnabled = false
        
        recordView.layer.cornerRadius = recordView.layer.frame.height / 2
        recordView.layer.masksToBounds = true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.clear
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleDimissKeyboard))
        self.view.addGestureRecognizer(tap)
        self.view.bindToKeyboard()
        
        SettingVC.registerDefaults()
        realm = try! Realm()
        
    }
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardView.isHidden = true
        CheckForEditedText()
        reTranslateSentence()
        
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        setupUIBtn()
        realm.refresh()

        speechRecognizer.delegate = self
        if let speechRecog = SFSpeechRecognizer(locale: Locale(identifier: DataAccess.availableLocales[indexOfLocaleSrc])) {
            self.speechRecognizer = speechRecog
        }
        
        checkForNetwork()
        authorizeAudioSession()
        requestAuthorizationSpeechRecognizer()
        
    }
    public override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
    }
    
    private func CheckForEditedText() {
        // Edit dataSource, tableViewCell after editing Text
        if let editedText = editedText, let indexPath = self.indexEditingText {
            let indexForRow = translatorList.count - 1 - indexPath.row
            let indexForDisplayingCell = (translatorList.count > 0 ) ? indexForRow : indexPath.row
            TranslationService.shared.translate(text: editedText, src: translatorList[indexForDisplayingCell].sourceLanguageTranslate, target: translatorList[indexForDisplayingCell].destinationLanguageTranslate) { (succ) in
                if succ {
                    try! self.realm.write {
                        self.translatorList[indexForDisplayingCell].sourceText = editedText
                        self.translatorList[indexForDisplayingCell].destinationText = TranslationService.shared.translatedText
                    }
                    
                    
                    let cell = self.tableView.cellForRow(at: indexPath) as! TranslateCell
                    cell.srcText.text = self.translatorList[indexPath.row].sourceText
                    cell.destText.text = self.translatorList[indexPath.row].destinationText

                    self.tableView.reloadData()
                }
            }
        }
    }
    
    private func setupUIBtn() {
        firstButton.setImage(UIImage(named: srcLanguage[1]), for: .normal)
        firstLabel.text = srcLanguage[2]
        
        secondButton.setImage(UIImage(named: destLanguage[1]), for: .normal)
        secondLabel.text = destLanguage[2]
    }
    private func checkForNetwork() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do {
            try reachability.startNotifier()
        } catch {
            print("Coud not start reachability Notifier")
        }
    }
    @objc func reachabilityChanged(note: Notification) {
        OperationQueue.main.addOperation {
            let reachability = note.object as! Reachability
            if UserDefaults.standard.bool(forKey: "useOnlyWifi") {
                switch reachability.connection {
                case .wifi:
                    self.isConnectionSatisfied = true
                    if SFSpeechRecognizer.authorizationStatus() == .authorized && AVAudioSession.sharedInstance().recordPermission() == .granted {
                        self.recordButton.isEnabled = true
                        self.keyboardBtn.isEnabled = true
                    }
                case .cellular:
                    self.alert(title: "Connection fail", message: "Enable Wifi to continue")
                    self.isConnectionSatisfied = false
                    self.recordButton.isEnabled = false
                    self.keyboardBtn.isEnabled = false
                case .none:
                    self.alert(title: "Connection fail", message: "Network is not reachable")
                    self.isConnectionSatisfied = false
                    self.recordButton.isEnabled = false
                    self.keyboardBtn.isEnabled = false
                }
            } else {
                switch reachability.connection {
                case .wifi:
                    self.isConnectionSatisfied = true
                    if SFSpeechRecognizer.authorizationStatus() == .authorized && AVAudioSession.sharedInstance().recordPermission() == .granted {
                        self.recordButton.isEnabled = true
                        self.keyboardBtn.isEnabled = true
                    } else {
                        self.alert(withMessage: "Access denied to Microphone and Speech Recognition.")
                    }
                case .cellular:
                    self.isConnectionSatisfied = true
                    if SFSpeechRecognizer.authorizationStatus() == .authorized && AVAudioSession.sharedInstance().recordPermission() == .granted {
                        self.recordButton.isEnabled = true
                        self.keyboardBtn.isEnabled = true
                    } else {
                        self.alert(withMessage: "Access denied to Microphone and Speech Recognition.")
                    }
                case .none:
                    self.alert(title: "Connection fail", message: "Network is not reachable")
                    self.isConnectionSatisfied = false
                    self.recordButton.isEnabled = false
                    self.keyboardBtn.isEnabled = false
                }
            }
        }
        
    }

    private func requestAuthorizationSpeechRecognizer() {
        SFSpeechRecognizer.requestAuthorization { authStatus in
            // The callback may not be called on the main thread.
            // Add an operation to the main queue to update the record button's state.
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    if AVAudioSession.sharedInstance().recordPermission() == .granted && self.isConnectionSatisfied {
                        self.recordButton.isEnabled = true
                        self.keyboardBtn.isEnabled = true
                    }
                case .denied:
                    self.recordButton.isEnabled = false
                    self.keyboardBtn.isEnabled = false
                    let message = "User denied access to speech recognition."
                    self.alert(withMessage: message)
                case .restricted:
                    self.recordButton.isEnabled = false
                    self.keyboardBtn.isEnabled = false
                    let message = "Speech recognition restricted on this device."
                    self.alert(withMessage: message)
                case .notDetermined:
                    self.recordButton.isEnabled = false
                    self.keyboardBtn.isEnabled = false
                    let message = "Speech recognition not yet authorized."
                    self.alert(withMessage: message)
                }
            }
        }
    }
    
    fileprivate func authorizeAudioSession() {
        OperationQueue.main.addOperation {
            switch AVAudioSession.sharedInstance().recordPermission() {
            case .granted:
                // we will enable recordbutton in authSpeechRecognizer later
                break
            case .denied:
                self.recordButton.isEnabled = false
                self.keyboardBtn.isEnabled = false
                let message = "User denied access to Microphone."
                self.alert(withMessage: message)
            case .undetermined:
                AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                    if granted {
                        print("Granted access to Microphone")
                        DispatchQueue.main.async {
                            self.recordButton.isEnabled = true
                            self.keyboardBtn.isEnabled = true
                        }
                    } else {
                        print("Granted access to Microphone")
                        DispatchQueue.main.async {
                            self.recordButton.isEnabled = false
                            self.keyboardBtn.isEnabled = false
                        }
                    }
                })
            }
        }
    }
    
    private func translateText(inputText: String, src: String, target: String) {
        TranslationService.shared.translate(text: inputText, src: src, target: target) { (success) in
            if success {
                self.translatedText = TranslationService.shared.translatedText
                
                if self.translatedText != "" {

                    let translatorItem = Translator()
                    translatorItem.sourceText = inputText
                    translatorItem.sourceCodeVoice = DataAccess.availableLocales[self.indexOfLocaleSrc]
                    translatorItem.sourceImageName = self.srcLanguage[1]
                    translatorItem.sourceName = self.srcLanguage[2]
                    translatorItem.sourceLanguageTranslate = self.srcLanguage[0]
                    
                    translatorItem.destinationText = self.translatedText
                    translatorItem.destinationCodeVoice = DataAccess.availableLocales[self.indexOfLocaleDest]
                    translatorItem.destinationImageName = self.destLanguage[1]
                    translatorItem.destinationName = self.destLanguage[2]
                    translatorItem.destinationLanguageTranslate = self.destLanguage[0]
                    
                    try! self.realm.write {
                        self.realm.add(translatorItem)
                    }
                    self.tableView.reloadData()
                    
                    if UserDefaults.standard.bool(forKey: "autoPronoun") {
                        guard let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? TranslateCell else { return }
                        cell.destSpeakBtnPressed(cell.destSpeakBtn)
                    }
                }
                
            }
        }
    }
    
    private func startRecording() throws {
        
        // Cancel the previous task if it's running.
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try audioSession.setMode(AVAudioSessionModeDefault)
            try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
            try audioSession.overrideOutputAudioPort(.speaker)
        } catch {
            print("audioSession properties weren't set because of an error.")
        }
        
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let recognitionRequest = recognitionRequest else { fatalError("Unable to created a SFSpeechAudioBufferRecognitionRequest object") }
        
        // Configure request so that results are returned before audio recording is finished
        recognitionRequest.shouldReportPartialResults = true
        
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in
            var isFinal = false
            if let result = result {
                self.originalText = result.bestTranscription.formattedString
                isFinal = result.isFinal
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                self.audioEngine.inputNode.removeTap(onBus: 0)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.recordButton.isEnabled = true
                self.recordButton.stopAnimation()
                
                self.keyboardBtn.isEnabled = true
                
                if self.originalText != "" {
                    self.translateText(inputText: self.originalText, src: self.srcLanguage[0], target: self.destLanguage[0])
                }
                
                
            } else {
                if error == nil {
                    self.restartSpeechTimeOut()
                } else {
                    // cancel voice recognition
                }
            }
        }
        let recordingFormat = audioEngine.inputNode.outputFormat(forBus: 0)
        audioEngine.inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        try audioEngine.start()
        
        changeBtnState(isEnable: false)
    }
    
    private func restartSpeechTimeOut() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(timeInterval: speechTimeOutInterval, target: self, selector: #selector(timerDidFinishTalk), userInfo: nil, repeats: false)
    }
    
    @objc func timerDidFinishTalk() {
        if UserDefaults.standard.bool(forKey: "detectEndOfSpeech") {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            timer?.invalidate()
            timer = nil
            changeBtnState(isEnable: true)
        }
        
    }
    func alert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: "App-Prefs:root=General") else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
            }
        }
        alert.addAction(settingsAction)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    fileprivate func alert(withMessage text: String) {
        let alert = UIAlertController(title: "Not authorized", message: text, preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
            }
        }
        alert.addAction(settingsAction)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    private func changeBtnState(isEnable: Bool) {
        firstButton.isEnabled = isEnable
        secondButton.isEnabled = isEnable
        exchangeLangBtn.isEnabled = isEnable
        
        settingBtn.isEnabled = isEnable
        suggestBtn.isEnabled = isEnable
        
        tableView.isUserInteractionEnabled = isEnable
    }
    
    private func reTranslateSentence() {
        OperationQueue.main.addOperation {
            if self.translatorList.count > 0 {
                for i in 0..<self.translatorList.count {
                    let element = self.translatorList[i]
                    if element.destinationText == "" {
                        TranslationService.shared.translate(text: element.sourceText, src: element.sourceLanguageTranslate, target: element.destinationLanguageTranslate, completion: { (success) in
                            if success {
                                try! self.realm.write {
                                    element.destinationText = TranslationService.shared.translatedText
                                }
                            }
                        })
                    }
                }
            }
            
            self.realm.refresh()
            self.tableView.reloadData()
        }
    }
    @objc func handleDimissKeyboard() {
        keyboardView.isHidden = true
        self.view.endEditing(true)
    }
    
    // ------- MARK: Interface Builder actions
    
    @IBAction func recordButtonTapped() {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            recordButton.isEnabled = false
            recordButton.stopAnimation()
            
            keyboardBtn.isEnabled = false
            
            // overhere, we need animation for record button as stopping state
            
            changeBtnState(isEnable: true)
        } else {
            try! startRecording()
            recordButton.startAnimation()
            originalText = ""
            translatedText = ""
        }
    }
    
    @IBAction func firstBtnPressed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toLanguageVC") as? LanguageVC else { return }
        vc.srcDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func secondBtnPressed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toLanguageVC") as? LanguageVC else { return }
        vc.destDelegate = self
        present(vc, animated: true, completion: nil)
    }
    @IBAction func changeBtnPressed(_ sender: UIButton) {
        let src = destLanguage
        let dest = srcLanguage
        srcLanguage = src
        destLanguage = dest
        let index = indexOfLocaleDest
        indexOfLocaleDest = indexOfLocaleSrc
        indexOfLocaleSrc = index
        
        setupUIBtn()
    }
    
    @IBAction func settingBtnPressed(_ sender: UIButton) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toSettingVC") as? SettingVC else { return }
        present(vc, animated: true, completion: nil)
    }
    @IBAction func suggestBtnPressed(_ sender: UIButton) {
        guard let suggestionVC = storyboard?.instantiateViewController(withIdentifier: "toSuggestionVC") as? SuggestionVC else { return }
        suggestionVC.suggestionDelegate = self
        present(suggestionVC, animated: true, completion: nil)
    }
    @IBAction func keyboardBtnPressed(_ sender: UIButton) {
        keyboardView.isHidden = false
        keyboardTF.becomeFirstResponder()
    }
    
    @IBAction func keyboardSendBtnPressed(_ sender: UIButton) {
        guard let text = keyboardTF.text else { return }
        if text != "" {
            translateText(inputText: text, src: srcLanguage[0], target: destLanguage[0])
            
            keyboardView.isHidden = true
            keyboardTF.text = ""
            keyboardTF.resignFirstResponder()
        }
        
    }
}

extension MainVC: SFSpeechRecognizerDelegate {
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            recordButton.isEnabled = true
            recordButton.setTitle("Start Recording", for: [])
            keyboardBtn.isEnabled = true
        } else {
            recordButton.isEnabled = false
            recordButton.setTitle("Recognition not available", for: .disabled)
            keyboardBtn.isEnabled = false
        }
    }
}
extension MainVC: SourceLanguageDelegate {
    func passInfo(str: Array<String>,withLocale index: Int) {
        self.srcLanguage = str
        let code = str[0]
        let name = str[2]
        indexOfLocaleSrc = DataAccess.languages.index( where: { $0 == [code,name] })!
    }
}
extension MainVC: DestinationLanguageDelegate {
    func passInformation(str: Array<String>, withLocale index: Int) {
        self.destLanguage = str
        let code = str[0]
        let name = str[2]
        indexOfLocaleDest = DataAccess.languages.index( where: { $0 == [code,name] })!
    }
}
extension MainVC: UITableViewDelegate, UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return translatorList.count
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? TranslateCell else { return UITableViewCell() }
        
        // Resever index to display Cell from top -> bottom whenever new translation comes up
        let index = translatorList.count - 1 - indexPath.row
        let indexPathForRow = (translatorList.count > 0) ? index : indexPath.row
        
        // srcText-srcCode-srcImageName-srcName
        cell.srcText.text = translatorList[indexPathForRow].sourceText
        cell.srcVoiceLanguageCode = translatorList[indexPathForRow].sourceCodeVoice
        cell.srcImage.image = UIImage(named: translatorList[indexPathForRow].sourceImageName)
        cell.srcLabelName.text = translatorList[indexPathForRow].sourceName
        
        cell.destText.text = translatorList[indexPathForRow].destinationText
        cell.destVoiceLanguageCode = translatorList[indexPathForRow].destinationCodeVoice
        cell.destImage.image = UIImage(named: translatorList[indexPathForRow].destinationImageName)
        cell.destLabelName.text = translatorList[indexPathForRow].destinationName

        cell.backgroundColor = UIColor.clear
        cell.delegate = self

        return cell
    }
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.6) {
            cell.contentView.alpha = 1.0
        }
    }
    public func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.6) {
            cell.contentView.alpha = 0.4
        }
    }
}
extension MainVC: TranslateCellDelegate {
    // MARK: - TranslateCellDelegate functions
    func deleteCellPressed(cell: TranslateCell) {
        guard var index = tableView.indexPath(for: cell) else { return }
        let alertVC = UIAlertController(title: "Delete this phrase permanently ?", message: nil, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (_) in
            
            // Need to reverse the index ( for displaying cell purpose)
            index.row = self.translatorList.count - 1 - index.row
            let item = self.translatorList[index.row]
            try! self.realm.write {
                self.realm.delete(item)
            }            
            
            self.tableView.deleteRows(at: [index], with: UITableViewRowAnimation.fade)
            self.tableView.reloadData()
        }))
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alertVC, animated: true, completion: nil)

    }
    func alertCopySuccess() {
        let successText = UILabel(frame: CGRect(x: self.view.frame.width / 2 - 100 , y: self.view.frame.height / 2 - 30, width: 200, height: 60))
        successText.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        successText.text = "Copied to Clipboard"
        successText.textColor = .white
        successText.textAlignment = .center
        successText.layer.cornerRadius = 10
        successText.layer.masksToBounds = true
        self.view.addSubview(successText)
        
        Timer.scheduledTimer(withTimeInterval: 2.5, repeats: false) { (_) in
            successText.removeFromSuperview()
        }
    }
    func fullscreenForText(str: String) {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toFullScreenVC") as? FullScreenVC else { return }
        vc.text = str
        self.present(vc, animated: true, completion: nil)
    }
    func shareText(str: String, sender: UIButton) {
        let activity = UIActivityViewController(activityItems: [str], applicationActivities: nil)
        activity.popoverPresentationController?.sourceView = sender
        self.present(activity, animated: true, completion: nil)
    }
    func textToSpeakForSrc(voiceLanguageCode: String, withText text: String, cell: TranslateCell) {
        textToSpeech(voiceLanguageCode: voiceLanguageCode, text: text, cell: cell, button: cell.srcSpeakBtn)
    }
    func textToSpeakForDest(voiceLanguageCode: String, withText text: String, cell: TranslateCell) {
        textToSpeech(voiceLanguageCode: voiceLanguageCode, text: text, cell: cell, button: cell.destSpeakBtn)
    }
    func edit(text: String, cell: TranslateCell) {
        indexEditingText = tableView.indexPath(for: cell)
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "toEditVC") as? EditVC else { return }
        vc.text = text
        vc.countryName = cell.srcLabelName.text
        vc.flagName = cell.srcImage.image
        vc.editDelegate = self
        present(vc, animated: true, completion: nil)
    }
    
    // -----
    private func textToSpeech(voiceLanguageCode: String, text: String, cell: TranslateCell, button: UIButton) {
        let languageCode = addMoreLanguage(checkWithLang: voiceLanguageCode)
        speechSynthesizer.delegate = self
        speechSynthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
        if tableView.visibleCells.count > 0 {
            for cell in tableView.visibleCells as! [TranslateCell] {
                cell.destSpeakBtn.setImage(UIImage(named:"speaker inactive"), for: .normal)
                cell.srcSpeakBtn.setImage(UIImage(named:"speaker inactive"), for: .normal)
            }
        }
        if !speechSynthesizer.isSpeaking {
            let speakUtterance = AVSpeechUtterance(string: text)
            speakUtterance.rate = UserDefaults.standard.float(forKey: "rateValue")  // 0.0 -> 1.0,  default : AVSpeechUtteranceDefaultSpeechRate
            speakUtterance.pitchMultiplier = UserDefaults.standard.float(forKey: "pitchValue")  // 0.5 -> 2.0,  default: 1.0
            speakUtterance.volume = UserDefaults.standard.float(forKey: "volumeValue")    //0.0 -> 1.0,   default : 1.0
            speakUtterance.voice = AVSpeechSynthesisVoice(language: languageCode)
            
            speechSynthesizer.speak(speakUtterance)
            button.setImage(UIImage(named:"speaker active"), for: .normal)
            
        } else {
            speechSynthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
            button.setImage(UIImage(named:"speaker inactive"), for: .normal)
        }
    }
    private func addMoreLanguage(checkWithLang languageCode: String) -> String {
        var code = languageCode
        let arrGerman = ["de-AT", "de-CH"]            // switch to : de-DE
        let arrEnglish = ["en-AE", "en-CA", "en-ID", "en-IN", "en-NZ", "en-PH", "en-SA", "en-SG"]                   // en-US
        let arrSpain = ["es-CL", "es-CO", "es-US"]  // es-ES
        let arrFrance = ["fr-BE", "fr-CH"]          // fr-FR
        let arrIndia = ["hi-IN-translit", "hi-Latn"] // hi-IN
        let arrChinese = ["wuu-CN", "yue-CN"]       // zh-CN
        
        for i in arrGerman {
            if languageCode == i {
                code = "de-DE"
            }
        }
        for i in arrEnglish {
            if languageCode == i {
                code = "en-US"
            }
        }
        for i in arrSpain {
            if languageCode == i {
                code = "es-ES"
            }
        }
        for i in arrFrance {
            if languageCode == i {
                code = "fr-FR"
            }
        }
        for i in arrIndia {
            if languageCode == i {
                code = "hi-IN"
            }
        }
        for i in arrChinese {
            if languageCode == i {
                code = "zh-CN"
            }
        }
        return code
    }
}
extension MainVC: EditTextDelegate {
    func editDone(withText text: String) {
        editedText = text
    }
}
extension MainVC: AVSpeechSynthesizerDelegate {
    public func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        if tableView.visibleCells.count > 0 {
            for cell in tableView.visibleCells as! [TranslateCell] {
                cell.destSpeakBtn.setImage(UIImage(named:"speaker inactive"), for: .normal)
                cell.srcSpeakBtn.setImage(UIImage(named:"speaker inactive"), for: .normal)
            }
        }
    }
}
extension MainVC: SuggestionDelagate {
    func didClickSuggestion(suggestion: UserSuggestion) {

        let newTranslator = Translator()
        newTranslator.sourceText = suggestion.sourceText
        newTranslator.sourceCodeVoice = suggestion.sourceCodeVoice
        newTranslator.sourceImageName = suggestion.sourceImageName
        newTranslator.sourceName = suggestion.sourceName
        newTranslator.sourceLanguageTranslate = suggestion.sourceLanguageTranslate
        
        newTranslator.destinationText = suggestion.destinationText
        newTranslator.destinationCodeVoice = suggestion.destinationCodeVoice
        newTranslator.destinationImageName = suggestion.destinationImageName
        newTranslator.destinationName = suggestion.destinationName
        newTranslator.destinationLanguageTranslate = suggestion.destinationLanguageTranslate
        try! self.realm.write {
            self.realm.add(newTranslator)
        }
    }
}
