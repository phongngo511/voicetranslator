//
//  LanguageVC.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 2/2/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class LanguageVC: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var srcDelegate: SourceLanguageDelegate?
    var destDelegate: DestinationLanguageDelegate?
    
    let cellId = "languageCell"
    var filteredAllLanguages: [[String]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filteredAllLanguages = DataAccess.languages
        
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        tableView.reloadData()
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleEndEdting))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func handleEndEdting() {
        view.endEditing(true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
    @IBAction func doneBtnPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
extension LanguageVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return DataAccess.recentLanguages.count
        } else {
            return filteredAllLanguages.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? LanguageCell else { return UITableViewCell() }
        
        if indexPath.section == 0 {
            let imgName = fetchImageLanguage(atIndexPath: indexPath, ofArray: DataAccess.recentLanguages)
            let name = DataAccess.recentLanguages[indexPath.row][1]
            var img = UIImage()
            if let image = UIImage(named: imgName) {
                img = image
            }
            cell.configCell(image: img, name: name)
        }
        if indexPath.section == 1 {
            let imgName = fetchImageLanguage(atIndexPath: indexPath, ofArray: filteredAllLanguages)
            let name = filteredAllLanguages[indexPath.row][1]
            var img = UIImage()
            if let image = UIImage(named: imgName) {
                img = image
            }
            cell.configCell(image: img, name: name)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Recently Used"
        }
        if section == 1 {
            return "All Lanaguages"
        }
        return ""
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let view = view as? UITableViewHeaderFooterView {
            view.backgroundView?.backgroundColor = #colorLiteral(red: 0.9602157195, green: 0.9602157195, blue: 0.9602157195, alpha: 1)
            view.textLabel?.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            let code = DataAccess.recentLanguages[indexPath.row][0]
            let name = DataAccess.recentLanguages[indexPath.row][1]
            let imageName = fetchImageLanguage(atIndexPath: indexPath, ofArray: DataAccess.recentLanguages)
          
            if let firstLanguageDelegate = srcDelegate {
                firstLanguageDelegate.passInfo(str: [code,imageName,name], withLocale: indexPath.row)
            }
            if let secondLanguageDelegate = destDelegate {
                secondLanguageDelegate.passInformation(str: [code,imageName,name], withLocale: indexPath.row)
            }

            dismiss(animated: true, completion: nil)
        }
        if indexPath.section == 1 {
            // add language to recent & recentFullName
            if !DataAccess.recentLanguages.joined().contains(filteredAllLanguages[indexPath.row][1]) {
                DataAccess.recentLanguages.append(filteredAllLanguages[indexPath.row])
            }
            
            let index = DataAccess.languages.index(where: { $0 == filteredAllLanguages[indexPath.row] })!
            
            let code = filteredAllLanguages[indexPath.row][0]
            let name = filteredAllLanguages[indexPath.row][1]
            let imageName = fetchImageLanguage(atIndexPath: indexPath, ofArray: filteredAllLanguages)
            if let firstLanguageDelegate = srcDelegate {
                firstLanguageDelegate.passInfo(str: [code,imageName,name], withLocale: index)
            }
            if let secondLanguageDelegate = destDelegate {
                secondLanguageDelegate.passInformation(str: [code,imageName,name], withLocale: index)
            }
            dismiss(animated: true, completion: nil)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func fetchImageLanguage(atIndexPath indexPath: IndexPath, ofArray arr: [[String]]) -> String {
        let index = arr[indexPath.row][1].index(of: "-")!
        
        let imgName = String(arr[indexPath.row][1].suffix(from: index))
        var literalImgName = String(imgName.dropFirst().dropFirst())
        if literalImgName == "India translit" {
            literalImgName = "India"
        }
        return literalImgName
    }
}

extension LanguageVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            filteredAllLanguages = DataAccess.languages
            tableView.reloadData()
            return
        }
        filteredAllLanguages = DataAccess.languages.filter( { (text : [String]) in
            text[1].lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
    
}


