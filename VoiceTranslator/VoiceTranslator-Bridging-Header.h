//
//  OpeanEarsHeader.h
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 4/4/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

#ifndef OpeanEarsHeader_h
#define OpeanEarsHeader_h

#import <OpenEars/OEPocketsphinxController.h>
#import <OpenEars/OELanguageModelGenerator.h>
#import <OpenEars/OEFliteController.h>
#import <Slt/Slt.h>
#import <OpenEars/OEEventsObserver.h>
#import <OpenEars/OELogging.h>
#import <OpenEars/OEAcousticModel.h>

#endif /* OpeanEarsHeader_h */
