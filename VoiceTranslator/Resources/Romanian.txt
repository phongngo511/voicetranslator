Puteți să-mi arătați pe hartă ?
Poți să mă duci la aeroport, te rog?
Îmi poți spune cum să ajung la stația de autobuz?
Știți unde e oficiul poștal?
Luați cărți de credit?
Cum funcționează sistemul școlar aici?
Cât costă asta ?
Sunt pierdut, am nevoie de ajutor.
Ce ai făcut astăzi pentru micul dejun?
Ce este acea clădire mare acolo?
Care este cea mai ciudată mâncare pe care o pot mânca aici?
Care este perioada preferată a anului aici?
Ce sport joacă copiii aici?
Unde pot găsi un autobuz / taxi?
De unde pot să iau ceva de mâncare?
Unde este un loc bun pentru a bea ceva aici?
Unde este cea mai apropiată baie?