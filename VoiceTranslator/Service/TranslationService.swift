//
//  TranslationService.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 2/2/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

let API_KEY = "AIzaSyBHjcWfUAj_U1yETrzJ1minPuZFu1TEIug"
typealias completionHandler = (_ success: Bool ) -> ()

class TranslationService {
    static let shared = TranslationService()
    
    var translatedText = ""
    
    func translate(text: String, src: String, target: String, completion: @escaping completionHandler ) {
        let url = "https://translation.googleapis.com/language/translate/v2?key=\(API_KEY)"
        let body : [String: String] = [
            "q": text,
            "source": src,
            "target": target,
            "format": "text"
        ]
        
        Alamofire.request(url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            
            if response.error == nil {
                guard let data = response.data else { return }
                do {
                    if let json = try JSON(data: data).dictionary {
                        if let data = json["data"]?.dictionary {
                            if let translation = data["translations"] {
                                self.translatedText = translation[0]["translatedText"].stringValue
                            }
                            completion(true)
                        }
                    }
                } catch {
                    print("error ko bat dc json")
                    completion(false)
                }
            } else {
                print(response.error as Any)
                completion(false)
            }
        }
    }
}

