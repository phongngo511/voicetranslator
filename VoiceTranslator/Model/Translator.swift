//
//  Translator.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 3/9/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import Foundation
import RealmSwift

class Translator: Object {
    @objc dynamic var sourceText = ""
    @objc dynamic var sourceCodeVoice = ""
    @objc dynamic var sourceImageName = ""
    @objc dynamic var sourceName = ""
    @objc dynamic var sourceLanguageTranslate = ""
    
    @objc dynamic var destinationText = ""
    @objc dynamic var destinationCodeVoice = ""
    @objc dynamic var destinationImageName = ""
    @objc dynamic var destinationName = ""
    @objc dynamic var destinationLanguageTranslate = ""
}

