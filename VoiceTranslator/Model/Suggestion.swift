//
//  Suggestion.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 3/16/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import Foundation
import RealmSwift

class RealmString: Object {
    @objc dynamic var stringValue = ""
    @objc dynamic var id = 0
    override static func primaryKey() -> String {
        return "id"
    }
}

class Suggestion: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    let sentencesList = List<RealmString>()
    var sentence: [String] {
        get {
            return sentencesList.map { $0.stringValue }
        }
        set {
            sentencesList.removeAll()
            sentencesList.append(objectsIn: newValue.map({ RealmString(value: [$0]) }))
        }
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
class UserSuggestion: Object {
    @objc dynamic var id = 0
    @objc dynamic var sourceText = ""
    @objc dynamic var sourceCodeVoice = ""
    @objc dynamic var sourceImageName = ""
    @objc dynamic var sourceName = ""
    @objc dynamic var sourceLanguageTranslate = ""
    
    @objc dynamic var destinationText = ""
    @objc dynamic var destinationCodeVoice = ""
    @objc dynamic var destinationImageName = ""
    @objc dynamic var destinationName = ""
    @objc dynamic var destinationLanguageTranslate = ""
    override class func primaryKey() -> String? {
        return "id"
    }
}
