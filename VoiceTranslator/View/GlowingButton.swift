//
//  GlowingButton.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 2/8/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit
class GlowingButton: UIButton {
    @IBInspectable var animDuration : CGFloat = 2
    @IBInspectable var cornerRadius : CGFloat = 10
    @IBInspectable var maxGlowSize : CGFloat = 30
    @IBInspectable var minGlowSize : CGFloat = 5
    @IBInspectable var color: CGColor = #colorLiteral(red: 0.26, green: 0.47, blue: 0.96, alpha: 1)
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentScaleFactor = UIScreen.main.scale
        self.layer.masksToBounds = false
        
    }
    
    func startAnimation() {
        // Setup Button
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowPath = CGPath(roundedRect: self.bounds, cornerWidth: cornerRadius, cornerHeight: cornerRadius, transform: nil)
        self.layer.shadowColor = color
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = maxGlowSize
        self.layer.shadowOpacity = 1
        
        // Add animation
        let layerAnimation = CABasicAnimation(keyPath: "shadowRadius")
        layerAnimation.fromValue = maxGlowSize
        layerAnimation.toValue = minGlowSize
        layerAnimation.autoreverses = true
        layerAnimation.isAdditive = false
        layerAnimation.duration = CFTimeInterval(animDuration/2)
        layerAnimation.fillMode = kCAFillModeForwards
        layerAnimation.isRemovedOnCompletion = false
        layerAnimation.repeatCount = .infinity
        self.layer.add(layerAnimation, forKey: "glowingAnimation")
        
    }
    
    func stopAnimation() {
        self.layer.shadowOpacity = 0
        self.layer.removeAnimation(forKey: "glowingAnimation")
    }
}
