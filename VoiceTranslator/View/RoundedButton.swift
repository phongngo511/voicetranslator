//
//  RoundedButton.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 2/2/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.borderWidth = 1.5
        self.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        self.layer.masksToBounds = true
    }
    
}

