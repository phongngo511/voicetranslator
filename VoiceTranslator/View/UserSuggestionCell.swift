//
//  UserSuggestionCell.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 3/27/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit

class UserSuggestionCell: UITableViewCell {

    @IBOutlet weak var sentenceLbl: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
