//
//  TranslateCell.swift
//  VoiceTranslator
//
//  Created by Phong Ngo Phu on 2/3/18.
//  Copyright © 2018 Phong Ngo Phu. All rights reserved.
//

import UIKit
import Speech

protocol TranslateCellDelegate: class {
    func deleteCellPressed(cell: TranslateCell)
    func alertCopySuccess()
    func fullscreenForText(str: String)
    func shareText(str: String, sender: UIButton)
    func textToSpeakForSrc(voiceLanguageCode: String, withText text: String, cell: TranslateCell)
    func textToSpeakForDest(voiceLanguageCode: String, withText text: String, cell: TranslateCell)
    func edit(text: String, cell: TranslateCell)
}

class TranslateCell: UITableViewCell {

    @IBOutlet weak var srcView: UIView!
    @IBOutlet weak var destView: UIView!
    
    @IBOutlet weak var srcText: UILabel!
    @IBOutlet weak var destText: UILabel!
    @IBOutlet weak var srcSpeakBtn: UIButton!
    @IBOutlet weak var destSpeakBtn: UIButton!
    
    @IBOutlet weak var srcStackView: UIStackView!
    @IBOutlet weak var destStackView: UIStackView!
    
    @IBOutlet weak var srcImage: UIImageView!
    @IBOutlet weak var destImage: UIImageView!
    
    @IBOutlet weak var srcLabelName: UILabel!
    @IBOutlet weak var destLabelName: UILabel!
    
    @IBOutlet weak var destDeleteBtn: UIButton!
    @IBOutlet weak var destCopyBtn: UIButton!
    
    @IBOutlet weak var srcStackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var destStackViewHeightConstraint: NSLayoutConstraint!
    
    var srcVoiceLanguageCode = "en_US"
    var destVoiceLanguageCode = "fr_FR"

    var groupSrcBtnState = false
    var groupDestBtnState = false
    
    var delegate: TranslateCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        srcSpeakBtn.setImage(UIImage(named: "speaker inactive"), for: .normal)
        destSpeakBtn.setImage(UIImage(named: "speaker inactive"), for: .normal)
        
        srcView.layer.cornerRadius = 15
        srcView.layer.masksToBounds = true
        if #available(iOS 11.0, *) {
            srcView.layer.maskedCorners =  [.layerMaxXMinYCorner]
        } else {
            srcView.roundCorners(corners: [.topRight], radius: 15)
        }
        
        destView.layer.cornerRadius = 15
        destView.layer.masksToBounds = true
        if #available(iOS 11.0, *) {
            destView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        } else {
            destView.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 15)
        }
        
        destDeleteBtn.layer.cornerRadius = 15
        destDeleteBtn.layer.masksToBounds = true
        if #available(iOS 11.0, *) {
            destDeleteBtn.layer.maskedCorners = [.layerMaxXMaxYCorner]
        } else {
            destDeleteBtn.roundCorners(corners: [.bottomRight], radius: 15)
        }
        
        destCopyBtn.layer.cornerRadius = 15
        destCopyBtn.layer.masksToBounds = true
        if #available(iOS 11.0, *) {
            destCopyBtn.layer.maskedCorners = [.layerMinXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }

        srcStackViewHeightConstraint.constant = 0
        destStackViewHeightConstraint.constant = 0
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(handleTap1(sender:)))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(handleTap2(sender:)))
        srcView.addGestureRecognizer(tap1)
        destView.addGestureRecognizer(tap2)
    }
    
    @objc func handleTap1(sender: UITapGestureRecognizer) {
        if !groupSrcBtnState {
            // turn on
            srcStackViewHeightConstraint.constant = 30
            self.frame.size.height += 30
            groupSrcBtnState = true
            
            if groupDestBtnState {
                destStackViewHeightConstraint.constant = 0
                self.frame.size.height -= 30
                groupDestBtnState = false
                if #available(iOS 11.0, *) {
                    destView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                } else {
                    destView.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 15)
                }
            }
        } else {
            // turn off
            srcStackViewHeightConstraint.constant = 0
            self.frame.size.height -= 30
            groupSrcBtnState = false
        }
    }
    @objc func handleTap2(sender: UITapGestureRecognizer) {
        if !groupDestBtnState {
            // turn on
            destStackViewHeightConstraint.constant = 30
            self.frame.size.height += 30
            groupDestBtnState = true
            
            if groupSrcBtnState {
                srcStackViewHeightConstraint.constant = 0
                self.frame.size.height -= 30
                groupSrcBtnState = false
            }
        } else {
            // turn off
            destStackViewHeightConstraint.constant = 0
            self.frame.size.height -= 30
            groupDestBtnState = false
        }
        
        
        if groupDestBtnState {
            if #available(iOS 11.0, *) {
                destView.layer.maskedCorners = []
            } else {
                destView.roundCorners(corners: [], radius: 15)
            }
        } else {
            if #available(iOS 11.0, *) {
                destView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            } else {
                destView.roundCorners(corners: [.bottomLeft,.bottomRight], radius: 15)
            }
        }
        
    }
    
    
    // For source
    @IBAction func srcCopyBtnPressed(_ sender: UIButton) {
        UIPasteboard.general.string = srcText.text
        delegate.alertCopySuccess()
    }
    @IBAction func srcEditBtnPressed(_ sender: UIButton) {
        delegate.edit(text: srcText.text!, cell: self)
    }
    @IBAction func srcShareBtnPressed(_ sender: UIButton) {
        delegate.shareText(str: srcText.text!, sender: sender)
    }
    @IBAction func srcFullScreenBtnPressed(_ sender: UIButton) {
        delegate.fullscreenForText(str: srcText.text!)
    }
    
    @IBAction func srcDeleteBtnPressed(_ sender: UIButton) {
        delegate.deleteCellPressed(cell: self)
    }
    @IBAction func srcSpeakBtnPressed(_ sender: UIButton) {
        srcSpeakBtn.setImage(UIImage(named: "speaker active"), for: .normal)
        delegate.textToSpeakForSrc(voiceLanguageCode: srcVoiceLanguageCode, withText: srcText.text!, cell: self)
    }
    
    // For Destination
    @IBAction func destCopyBtnPressed(_ sender: UIButton) {
        UIPasteboard.general.string = destText.text
        delegate.alertCopySuccess()
    }
    @IBAction func destShareBtnPressed(_ sender: UIButton) {
        delegate.shareText(str: destText.text!, sender: sender)
    }
    @IBAction func destFullScreenBtnPressed(_ sender: UIButton) {
        delegate.fullscreenForText(str: destText.text!)
    }
    @IBAction func destDeleteBtnPressed(_ sender: UIButton) {
        delegate.deleteCellPressed(cell: self)
    }
    @IBAction func destSpeakBtnPressed(_ sender: UIButton) {
        destSpeakBtn.setImage(UIImage(named: "speaker active"), for: .normal)
        delegate.textToSpeakForDest(voiceLanguageCode: destVoiceLanguageCode, withText: destText.text!, cell: self)
    }
}
